# Senior Front-end test

Create a single page web app that loads JSON with Ajax and displays data.

[JSON data](./data.json)

## Minimum requirement to complete the test

- The user needs to be able to navigate the data without reloading the whole page.
- The app needs to work offline if it has been exposed to the web once and managed to locally store both its code and data.
- The application state needs to be managed with Redux.
- The design needs to work on mobile, small screen by default and build up from there for larger screen sizes.
- You may use web based resources to write your code.
- You are not allowed to ask someone else to help you write this code.
- You need to document how to install and run your code.

## Technical stack

You must use all parts of the technical stack below to achieve the above requirements.

- ES6
- Sass
- webpack
- React.js
- Redux
- Unit tests with Jest
- Test coverage report
- Minimum average coverage of 70%

## Justify your programming choices

We will discuss your code so you need to think about why you make programming choices and how to justify them.
